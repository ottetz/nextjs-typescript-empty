import Link from 'next/link';
import React from 'react';

import { Button } from '@flippingbook/web.react-kit/dist/Button';

import Layout from '../components/Layout';

const Header: React.SFC<Header.Props> = props => (
    <h1>{props.text}</h1>
);

namespace Header {
    export interface Props {
        text: string;
    }
}

export default () => (
    <Layout title="Home | Next.js + TypeScript Example">
        <Header text="Hello Next.js 👋" />
        <p>
            <Link href="/about">
                <a>
                    About
                </a>
            </Link>
        </p>
        <p>
            <Button>
                I'm react-kit button
            </Button>
        </p>
    </Layout>
);
