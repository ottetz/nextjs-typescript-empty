import Link from 'next/link';
import React from 'react';

export default class Page2 extends React.Component {
    render () {
        return (
            <div>
                <p>
                    page 2
                </p>
                <p>
                    <Link href="/">
                        <a>to index</a>
                    </Link>
                </p>
                <p>
                    <Link href="/about">
                        <a>to about</a>
                    </Link>
                </p>
            </div>
        );
    }
}
