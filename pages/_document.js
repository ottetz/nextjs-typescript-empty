//@ts-check
import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';

export default class SiteDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return { ...initialProps }
    }

    render() {
        return (
            <html>
                <Head>
                    <link rel="preconnect" href="https://fonts.gstatic.com"/>
                    <link rel="preconnect" href="https://fonts.googleapis.com"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
                </Head>
                <body>
                    <div className="root">
                        <Main />
                    </div>
                    <NextScript />
                </body>
            </html>
        )
    }
}
