import Link from 'next/link';
import React from 'react';

import Layout from '../components/Layout';

export default () => (
    <Layout title="About | Next.js + TypeScript Example">
        <p>This is the about page</p>
        <p>
            <Link href="/index">
                <a>
                    Go home
                </a>
            </Link>
        </p>
    </Layout>
);
