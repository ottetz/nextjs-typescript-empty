const withPlugins = require('next-compose-plugins');
const withSourceMaps = require('@zeit/next-source-maps');
const withCss = require('@zeit/next-css');
const withImages = require('next-images')
const withTypescript = require('./next-plugins/with-typescript');
const withBundleAnalizer = require('./next-plugins/with-bundle-analyzer');
const withLodashEs = require('./next-plugins/with-lodash-es');

// const withTM = require('next-plugin-transpile-modules');

const withTM = require('./next-plugins/with-transpile-modules');

const nextConfig = {
    webpack: (config, options) => {
        // Fixes npm packages that depend on `fs` module
        config.node = {
            fs: 'empty'
        };

        return config;
    }
}

const plugins = [
    withBundleAnalizer,
    withSourceMaps,
    withTypescript,
    withImages,
    withCss,
    withLodashEs,
    withTM,
    // [withTM, {
    //     transpileModules: ['lodash-es']
    // }],
];

module.exports = withPlugins(plugins, nextConfig);
