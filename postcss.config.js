module.exports = {
    plugins: {
        'postcss-import': { },
        'postcss-nested-ancestors': { },
        'postcss-nested': { },
        'postcss-cssnext': {
            features: {
                rem: false,
            },
        },
    },
};
