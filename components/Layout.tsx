import Head from 'next/head';
import Link from 'next/link';
import React from 'react';

namespace Layout {
    export type Props = {
        title?: string,
    };
}

class Layout extends React.Component<Layout.Props>{
    static defaultProps = {
        title: 'This is the default title',
    };

    componentDidMount () {
        if (!document.documentElement.classList.contains('wf-active')) {
            import('webfontloader')
                .then(WebFont => WebFont.load({
                    events: false,
                    google: {
                        families: [
                            'Roboto:300,400,500,700',
                            'Roboto Slab:300',
                        ],
                    },
                }));
        }
    }

    render () {
        return (
            <div>
                <Head>
                    <title>{this.props.title} | My site</title>
                </Head>

                <header>
                    <nav>
                        <Link href="/"><a>Home</a></Link> |{' '}
                        <Link href="/about"><a>About</a></Link> |{' '}
                        <Link href="/2"><a>Page 2</a></Link>
                    </nav>
                </header>

                {this.props.children}

                <footer>
                    (footer)
                    I'm here to stay
                </footer>
            </div>
        );
    }
}

export default Layout;
