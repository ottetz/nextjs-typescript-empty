module.exports = (nextConfig = {}) => {
    return Object.assign({}, nextConfig, {
        webpack(config, options) {
            const { isServer } = options;
            if (isServer) {
                config.externals = Array.isArray(config.externals)
                    ? config.externals.map(fn => {
                        if (typeof fn === "function") {
                            return (context, request, callback) => {
                                console.log('222222222222', request);
                                // We use lodash-es in the browser for tree-shaking, but
                                // switch to the regular lodash on the server to avoid having
                                // to transpile `import`/`export` there.

                                if (request.startsWith('lodash-es')) {
                                    console.log('patching lodash-es call');
                                    return callback(null, `commonjs ${request.replace('lodash-es', 'lodash')}`);
                                }

                                return fn(context, request, callback);
                            }
                        }

                        console.log('33333333333');
                        return fn;
                    })
                    : config.externals;
            }

            if (typeof nextConfig.webpack === 'function') {
                return nextConfig.webpack(config, options)
            }

            return config;
        }
    });
}
