module.exports = (nextConfig = {}) => {
    return Object.assign({}, nextConfig, {
        webpack(config, options) {
            const { ANALYZE } = process.env;
            const { isServer } = options;

            if (ANALYZE) {
                const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

                config.plugins.push(new BundleAnalyzerPlugin({
                    analyzerMode: 'server',
                    analyzerPort: isServer ? 8888 : 8889,
                    openAnalyzer: true
                }));
            }

            if (typeof nextConfig.webpack === 'function') {
                return nextConfig.webpack(config, options)
            }

            return config;
        }
    });
}
