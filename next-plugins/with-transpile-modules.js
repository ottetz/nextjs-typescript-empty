const resolve = require('resolve')

module.exports = (nextConfig = {}) => {
    return Object.assign({}, nextConfig, {
        webpack(config, options) {
            const { dir, isServer } = options;
            config.externals = [];

            // if (isServer) {
            //     config.externals.push((context, request, callback) => {
            //         console.log('!!', request);

            //         resolve(request, { basedir: dir, preserveSymlinks: true }, (err, res) => {
            //             console.log('!!!', request, '!', dir);

            //             if (err) {
            //                 return callback();
            //             }

            //             // Next.js by default adds every module from node_modules to
            //             // externals on the server build. This brings some undesirable
            //             // behaviors because we can't use modules that require CSS files like
            //             // `former-kit-skin-pagarme`.
            //             //
            //             // The lines below blacklist webpack itself (that cannot be put on
            //             // externals) and `former-kit-skin-pagarme`.
            //             if (
            //                 res.match(/node_modules[/\\].*\.js/)
            //                 && !res.match(/node_modules[/\\]webpack/)
            //                 && !res.match(/node_modules[/\\]@flippingbook/)
            //             ) {
            //                 return callback(null, `commonjs ${request}`);
            //             }

            //             callback();
            //         })
            //     })
            // }

            if (typeof nextConfig.webpack === 'function') {
                return nextConfig.webpack(config, options);
            }

            return config;
        }
    })
}
